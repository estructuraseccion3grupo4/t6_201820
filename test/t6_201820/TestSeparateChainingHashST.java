package t6_201820;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import VO.VORoute;
import data_structures.SeparateChainingHashST;
import logic.CapacidadException;

class TestSeparateChainingHashST
{
	class clasePrueba 
	{
		int id;
		String cadena;

		public clasePrueba(int id, String cadena)
		{
			this.id = id;
			this.cadena = cadena;
		}
	}

	//-----------------------------------------------------------
	// Attributes
	//-----------------------------------------------------------

	private SeparateChainingHashST<Integer, clasePrueba> list;

	//-----------------------------------------------------------
	// Scenarios
	//-----------------------------------------------------------

	void setupScenario0()
	{
		list = new SeparateChainingHashST<>(3);
	}
	/**
	 * Todas tienen diferentes estaciones de inicio
	 * @throws CapacidadException
	 */

	void setupScenario1()
	{
		list = new SeparateChainingHashST<>(3);

		clasePrueba prueba1 = new clasePrueba(9876, "miau");
		clasePrueba prueba2 = new clasePrueba(6786, "guaf");
		clasePrueba prueba3 = new clasePrueba(6969, "mielosito");

		list.put(prueba1.id, prueba1);
		list.put(prueba2.id, prueba2);
		list.put(prueba3.id, prueba3);

	}

	@Test
	void testPut()
	{
		try
		{
			setupScenario0();
			list.put(2878,new clasePrueba(2897, "jojk"));
			assertEquals(1, list.size(), "Se agrego correctamente");

		}
		catch(Exception e)
		{
			fail("No deberia generar excepcion");
		}
	}

	@Test
	void testGet() 
	{
		setupScenario0();
		assertEquals(new clasePrueba(000, "hujk"), list.get(6789), "No existe");
		
		setupScenario1();
		assertEquals(new clasePrueba(6789, "guaf"), list.get(6786), "Es correcto");

}

@Test
void testDelete() {
	fail("Not yet implemented");
}

}
