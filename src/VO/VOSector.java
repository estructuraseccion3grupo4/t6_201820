package VO;

public class VOSector {

	//--------------------------------------------------
	//Atributos
	//--------------------------------------------------

	private int id;

	private double LatitudMax;

	private double LatitudMin;

	private double LongitudMax;

	private double LongitudMin;

	//--------------------------------------------------
	//Atributos
	//--------------------------------------------------

	/**
	 * @param id
	 * @param latitudMax
	 * @param latitudMin
	 * @param longitudMax
	 * @param longitudMin
	 */
	public VOSector(int id, double latitudMax, double latitudMin, double longitudMax, double longitudMin) {
		this.id = id;
		LatitudMax = latitudMax;
		LatitudMin = latitudMin;
		LongitudMax = longitudMax;
		LongitudMin = longitudMin;
	}

	//--------------------------------------------------
	//Atributos
	//--------------------------------------------------

	/**
	 * @return the id
	 */
	public int darId() {
		return id;
	}

	/**
	 * @return the latitudMax
	 */
	public double darLatitudMax() {
		return LatitudMax;
	}

	/**
	 * @return the latitudMin
	 */
	public double darLatitudMin() {
		return LatitudMin;
	}

	/**
	 * @return the longitudMax
	 */
	public double darLongitudMax() {
		return LongitudMax;
	}

	/**
	 * @return the longitudMin
	 */
	public double darLongitudMin() {
		return LongitudMin;
	}



}
