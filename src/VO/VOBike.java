package VO;

public class VOBike {

	private int bikeId;
    private int totalTrips;
    private double totalDistance;
    private int totalDuration;

    public VOBike(int bikeId, int totalTrips, double totalDistance, int ptotalDuration) {
        this.bikeId = bikeId;
        this.totalTrips = totalTrips;
        this.totalDistance = totalDistance;
        this.totalDuration = ptotalDuration;
    }

    public int getBikeId() {
        return bikeId;
    }

    public int getTotalTrips() {
        return totalTrips;
    }

    public double getTotalDistance() {
        return totalDistance;
    }
    public int getTotalDuration() {
    	return totalDuration;
    }
    
    public void addTrip() {
    	totalTrips ++;
    }

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		VOBike bike = (VOBike) obj;
		return bike.getBikeId() == bikeId;
	}
    
    
 }
