package VO;

import java.util.Iterator;
import java.util.regex.Pattern;

import data_structures.List;

public class VORoute implements Comparable<VORoute> {

	//-----------------------------------------------------------
	// Atributos
	//-----------------------------------------------------------

	private String routeType;

	private List<String> geom;

	private String referenceStreetName;

	private String limitStreet1;

	private String limitStreet2;

	private String length;

	private String id;

	private double latitudMin;

	private double latitudMax;

	private double longitudMin;

	private double longitudMax;


	//-----------------------------------------------------------
	// Constructor
	//-----------------------------------------------------------

	/**
	 * @param routeType
	 * @param route
	 * @param referenceStreetName
	 * @param limitStreet1
	 * @param limitStreet2
	 * @param length
	 */
	public VORoute(String id,String routeType, List<String> route, String referenceStreetName, String limitStreet1,
			String limitStreet2, String length) 
	{
		this.id= id;
		this.routeType = routeType;
		this.geom = route;
		this.referenceStreetName = referenceStreetName;
		this.limitStreet1 = limitStreet1;
		this.limitStreet2 = limitStreet2;
		this.length = length;
		
		Iterator<String> iter = geom.iterator();
		double latMin = Double.MAX_VALUE;
		double latMax = Double.MIN_VALUE;
		double longMin = Double.MAX_VALUE;
		double longMax = Double.MIN_VALUE;
		
		while(iter.hasNext()) {
			String coor = iter.next();
			double longitud = Double.valueOf(coor.trim().split(" ")[0]);
			double latitud = Double.valueOf(coor.trim().split(" ")[1]);
			
			if(longitud < longMin) longMin = longitud;
			if(longitud > longMax) longMax = longitud;
			
			if(latitud < latMin) latMin = latitud;
			if(latitud > latMax) latMax = latitud;
		}
		
		latitudMin = latMin;
		latitudMax = latMax;
		longitudMin = longMin;
		longitudMax = longMax;
	}

	//-----------------------------------------------------------
	// Métodos
	//-----------------------------------------------------------
	/**
	 * @return the id
	 */
	public String getId()
	{
		return id;
	}

	/**
	 * @return the routeType
	 */
	public String getRouteType() {
		return routeType;
	}

	/**
	 * @return the route
	 */
	public List<String> getRoute() {
		return geom;
	}

	/**
	 * @return the referenceStreetName
	 */
	public String getReferenceStreetName() {
		return referenceStreetName;
	}

	/**
	 * @return the limitStreet1
	 */
	public String getLimitStreet1() {
		return limitStreet1;
	}

	/**
	 * @return the limitStreet2
	 */
	public String getLimitStreet2() {
		return limitStreet2;
	}

	/**
	 * @return the length
	 */
	public String getLength() {
		return length;
	}
	public double getLatitudMin()
	{
		return latitudMin;
	}
	public double getLatitudMax()
	{
		return latitudMax;
	}
	public double getLongitudMin()
	{
		return longitudMin;
	}
	public double getLongitudMax()
	{
		return longitudMax;
	}
	
	@Override
	public int compareTo(VORoute o) {
		// TODO Auto-generated method stub
		return 0;
	}


}
