package Controller;

import logic.Manager;

public class Controller {

	private static Manager manager = new Manager();
	
	public static int loadRoutes() {
		return manager.loadRoutes();
	}
}
