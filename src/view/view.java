package view;

import java.time.LocalDateTime;
import java.util.Scanner;

import Controller.Controller;

public class view {

	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		while(!fin)
		{
			printMenu();

			int option = sc.nextInt();
			int number = 0;

			switch(option)
			{
				case 1:
					System.out.println("Se cargaron " + Controller.loadRoutes() + " rutas.");
					break;
					
				case 4:	
					fin=true;
					sc.close();
					break;
					
				default:
					System.out.println("La opción no es válida");
			}
		}
	}

	private static void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 4----------------------");
		System.out.println("1. Cree una nueva coleccion de viajes");
		System.out.println("2. Crear heap.");
		System.out.println("3. Crear cola de prioridad.");
		System.out.println("4. Salir");
		System.out.println("Digite el n�mero de opci�n para ejecutar la tarea, luego presione enter: (Ej., 1):");
	}
	
}
