package data_structures;

import java.util.Iterator;

public class LinearProbingHashST<K extends Comparable<K>, V> implements ILinearProbingHashST<K, V> {

	private int n;
	private int M;
	private Object[] keys;
	private Object[] values;
	
	@SuppressWarnings("unchecked")
	public LinearProbingHashST(int capacity) {
		M = capacity;
		n = 0;
		keys = new Object[M];
		values = new Object[M];
	}
	
	public int size() {
		return n;
	}
	
	public boolean isEmpty() {
		return n == 0;
	}
	
	private int hash(K key) {
		return ((key.hashCode()) & 0x7fffffff) % M;
	}
	
	private boolean contains(K key) {
		return get(key) != null;
	}
	
	@SuppressWarnings("unchecked")
	private void resize(int capacity) {
		LinearProbingHashST<K, V> temp = new LinearProbingHashST<>(capacity);
		for(int i = 0; i < M; i++) {
			if(keys[i] != null) {
				temp.put((K) keys[i], (V) values[i]);
			}
		}
		
		keys = temp.keys;
		values = temp.values;
		M = temp.M;
	}
	
	@Override
	public void put(K key, V value) {
		if (n/M >= 0.75) resize(2*M);
		
		int i;
		for(i = hash(key); keys[i] != null; i = (i+1) % M) {
			if(keys[i].equals(key)) {
				values[i] = value;
				break;
			}
		}
		keys[i] = key;
		values[i] = value;
	}

	@SuppressWarnings("unchecked")
	@Override
	public V get(K key) {
		for(int i = hash(key); keys[i] != null; i = (i+1) % M) {
			if(key.equals(keys[i])) {
				return (V) values[i];
			}
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public V delete(K key) {
		if(!contains(key)) {
			return null;
		}
		
		//Buscar indice i de la llave.
		int i = hash(key);
		while(!key.equals(keys[i])) {
			i = (i + 1) % M;
		}
		
		//Borrar la llave y el valor
		V value = (V) values[i];
		values[i] = null;
		keys[i] = null;
		
		//Rehash
		i = (i + 1) % M;
		while(keys[i] != null) {
			K keyRehash = (K) keys[i];
			V valueRehash = (V) values[i];
			n--;
			put(keyRehash, valueRehash);
			
			i = (i + 1) % M;
		}
		n--;
		
		//Resize
		if(n > 0 && n <= M/8) resize(M/2);
		
		return value;
	}

	@Override
	public Iterator<K> iterator() {
		return new Iterator<K>() {
			K act = null;
			int i = -1;

			@Override
			public boolean hasNext() {
				if(isEmpty()) {
					return false;
				}
				
				if(act == null) {
					return true;
				}
				
				return ++i < n;
			}

			@SuppressWarnings("unchecked")
			@Override
			public K next() {
				while(keys[i] == null) {
					i++;
				}
				
				act = (K) keys[i];
				
				return act;
			}
		};
	}

}
