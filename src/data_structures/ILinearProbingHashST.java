package data_structures;

import java.util.Iterator;

public interface ILinearProbingHashST<K extends Comparable<K>, V> {

	void put(K key, V value);
	
	V get(K key);
	
	V delete(K key);
	
	Iterator<K> iterator();
}
