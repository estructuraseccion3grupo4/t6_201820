package data_structures;
import java.util.Iterator;

public interface ISeparateChainingHashST <Key extends Comparable<Key>, value> 
{
	public void put (Key k, value v);
	public value get (Key k);
	public value delete (Key k);
	public Iterator <Key> keys();
}
