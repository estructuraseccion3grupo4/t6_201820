package logic;

import data_structures.Stack;
import data_structures.IList;
import data_structures.LinearProbingHashST;
import data_structures.List;
import data_structures.SeparateChainingHashST;
import view.view;

import java.io.FileReader;
import java.time.LocalDateTime;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import VO.VORoute;
import VO.VOSector;
import VO.VOTrip;

public class Manager 
{
	class Sectorizacion {
		LinearProbingHashST<Integer, VOSector> sectores;
		
		private double LatitudMax;

		private double LatitudMin;

		private double LongitudMax;

		private double LongitudMin;
		
		public Sectorizacion(LinearProbingHashST<Integer, VOSector> sectores, double latitudMax, double latitudMin,
				double longitudMax, double longitudMin) {
			this.sectores = sectores;
			LatitudMax = latitudMax;
			LatitudMin = latitudMin;
			LongitudMax = longitudMax;
			LongitudMin = longitudMin;
		}
	}
	
	public final static String BIKE_ROUTES_FILE = "./src/data/CDOT_Bike_Routes_2014_1216-transformed.json";
	
	private List<VORoute> routes = new List<>();
	private Sectorizacion sectorizacion;
	
	public int loadRoutes()
	{
		try 
		{
			JSONParser parser = new JSONParser();

			JSONArray objArray = (JSONArray) parser.parse(new FileReader(BIKE_ROUTES_FILE));
			
			double longMin = Double.MAX_VALUE;
			double longMax = Double.MIN_VALUE;
			double latMin = Double.MAX_VALUE;
			double latMax = Double.MIN_VALUE;
			LinearProbingHashST<Integer, VOSector> sectores = new LinearProbingHashST<>(100);
		
			for(Object o: objArray)
			{
				JSONObject obj = (JSONObject) o;
				
				List<String> coors = new List<>();
				String geom = String.valueOf(obj.get("the_geom"));
				String[] listCoors = geom.substring(12, geom.length() - 1).split(",");
				
				for(int i = 0; i < listCoors.length; i++) {
					coors.add(listCoors[i]);
				}
				
				VORoute route = new VORoute(String.valueOf(obj.get("id")), String.valueOf(obj.get("BIKEROUTE")), coors, String.valueOf(obj.get("STREET")), String.valueOf(obj.get("F_STREET")),String.valueOf(obj.get("T_STREET")), String.valueOf(obj.get("Shape_Leng")));
				routes.add(route);
				
				if(route.getLongitudMax() > longMax) longMax = route.getLongitudMax();
				if(route.getLongitudMin() < longMin) longMin = route.getLongitudMin();
				if(route.getLatitudMax() > longMax) latMax = route.getLatitudMax();
				if(route.getLatitudMin() < latMin) latMin = route.getLatitudMin();
			}
			
			double dLong = (longMax - longMin)/10;
			double dLat = (latMax - latMin)/10;
			double latitud = latMax;
			double longitud = longMin;
			
			for(int i = 1; i <= 100; i++) {
				VOSector sector = new VOSector(i, latitud, latitud - dLat, longitud + dLong, longitud);
				
				if(i % 10 == 0) {
					latitud -= dLat;
					longitud = longMin;
				} else {
					longitud += dLong;
				}
				
				sectores.put(i, sector);
			}
			
			sectorizacion = new Sectorizacion(sectores, latMax, latMin, longMax, longMin);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			System.out.println("Se produjo un error. No se pudieron cargar las rutas");
			System.out.println(e.getMessage());
		}
		
		return routes.size();
	}
	
	public SeparateChainingHashST buscarCiclorrutasLocalizacionSeparate(double longi, double lati)
	{
		
	}
	
	
}
